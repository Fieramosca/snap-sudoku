module TestSudokuBoard where

import           Data.Sequence        (fromList)
import qualified Data.Sequence        as Seq
import           SudokuBoard
import           SudokuBoard.Internal
import           Test.HUnit

sampleRows :: Rows
sampleRows = fromList . map fromList $
    [ [ 0, 8, 0, 0, 0, 3, 0, 0, 0 ]
    , [ 6, 5, 9, 2, 0, 0, 7, 3, 1 ]
    , [ 0, 0, 0, 1, 0, 9, 4, 5, 0 ]
    , [ 0, 0, 6, 7, 0, 0, 0, 0, 0 ]
    , [ 3, 0, 2, 0, 1, 0, 8, 0, 5 ]
    , [ 0, 0, 0, 0, 0, 4, 6, 0, 0 ]
    , [ 0, 2, 4, 6, 0, 7, 0, 0, 0 ]
    , [ 7, 9, 3, 0, 0, 2, 1, 8, 6 ]
    , [ 0, 0, 0, 3, 0, 0, 0, 7, 0 ]
    ]

sampleCols :: Columns
sampleCols = fromList . map fromList $
    [ [ 0, 6, 0, 0, 3, 0, 0, 7, 0 ]
    , [ 8, 5, 0, 0, 0, 0, 2, 9, 0 ]
    , [ 0, 9, 0, 6, 2, 0, 4, 3, 0 ]
    , [ 0, 2, 1, 7, 0, 0, 6, 0, 3 ]
    , [ 0, 0, 0, 0, 1, 0, 0, 0, 0 ]
    , [ 3, 0, 9, 0, 0, 4, 7, 2, 0 ]
    , [ 0, 7, 4, 0, 8, 6, 0, 1, 0 ]
    , [0, 3, 5, 0, 0, 0, 0, 8, 7 ]
    , [0, 1, 0, 0, 5, 0, 0, 6, 0 ]
    ]

sampleBoxes :: Boxes
sampleBoxes = fromList . map fromList $
    [ [ 0, 8, 0, 6, 5, 9, 0, 0, 0 ]
    , [ 0, 0, 3, 2, 0, 0, 1, 0, 9 ]
    , [ 0, 0, 0, 7, 3, 1, 4, 5, 0 ]
    , [ 0, 0, 6, 3, 0, 2, 0, 0, 0 ]
    , [ 7, 0, 0, 0, 1, 0, 0, 0, 4 ]
    , [ 0, 0, 0, 8, 0, 5, 6, 0, 0 ]
    , [ 0, 2, 4, 7, 9, 3, 0, 0, 0 ]
    , [ 6, 0, 7, 0, 0, 2, 3, 0, 0 ]
    , [ 0, 0, 0, 1, 8, 6, 0, 7, 0 ]
    ]

sampleBoard :: Board
sampleBoard = Board {rows = sampleRows, columns = sampleCols, boxes = sampleBoxes}

sampleInsertedRows :: Rows
sampleInsertedRows = fromList . map fromList $
    [ [ 0, 8, 0, 0, 0, 3, 0, 0, 0 ]
    , [ 6, 5, 9, 2, 0, 0, 7, 3, 1 ]
    , [ 0, 1, 0, 1, 0, 9, 4, 5, 0 ]
    , [ 0, 0, 6, 7, 0, 0, 0, 0, 0 ]
    , [ 3, 0, 2, 0, 1, 0, 8, 0, 5 ]
    , [ 0, 0, 0, 0, 0, 4, 6, 0, 0 ]
    , [ 0, 2, 4, 6, 0, 7, 0, 0, 0 ]
    , [ 7, 9, 3, 0, 0, 2, 1, 8, 6 ]
    , [ 0, 0, 0, 3, 0, 0, 0, 7, 0 ]
    ]
sampleInsertedCols :: Columns
sampleInsertedCols = fromList . map fromList $
    [ [ 0, 6, 0, 0, 3, 0, 0, 7, 0 ]
    , [ 8, 5, 1, 0, 0, 0, 2, 9, 0 ]
    , [ 0, 9, 0, 6, 2, 0, 4, 3, 0 ]
    , [ 0, 2, 1, 7, 0, 0, 6, 0, 3 ]
    , [ 0, 0, 0, 0, 1, 0, 0, 0, 0 ]
    , [ 3, 0, 9, 0, 0, 4, 7, 2, 0 ]
    , [ 0, 7, 4, 0, 8, 6, 0, 1, 0 ]
    , [ 0, 3, 5, 0, 0, 0, 0, 8, 7 ]
    , [ 0, 1, 0, 0, 5, 0, 0, 6, 0 ]
    ]

sampleInsertedBoxes :: Boxes
sampleInsertedBoxes = fromList . map fromList $
    [ [ 0, 8, 0, 6, 5, 9, 0, 1, 0 ]
    , [ 0, 0, 3, 2, 0, 0, 1, 0, 9 ]
    , [ 0, 0, 0, 7, 3, 1, 4, 5, 0 ]
    , [ 0, 0, 6, 3, 0, 2, 0, 0, 0 ]
    , [ 7, 0, 0, 0, 1, 0, 0, 0, 4 ]
    , [ 0, 0, 0, 8, 0, 5, 6, 0, 0 ]
    , [ 0, 2, 4, 7, 9, 3, 0, 0, 0 ]
    , [ 6, 0, 7, 0, 0, 2, 3, 0, 0 ]
    , [ 0, 0, 0, 1, 8, 6, 0, 7, 0 ]
    ]

sampleInsertedBoard :: Board
sampleInsertedBoard = Board {rows = sampleInsertedRows, columns = sampleInsertedCols, boxes = sampleInsertedBoxes}

sampleFullRows :: Rows
sampleFullRows = Seq.replicate 9 (Seq.replicate 9 1)

sudokuBoardTests =
    TestList [ "testMakeColumnsFromRows" ~: sampleCols ~=? makeColumnsFromRows sampleRows
            , "testMakeBoxesFromRows" ~: sampleBoxes ~=? makeBoxesFromRows sampleRows
            , "testInitializeBoardFromRows" ~: sampleBoard ~=? initializeBoardFromRows sampleRows
            , "testBoardLookup" ~:  8 ~=? boardLookup sampleBoard (1, 0)
            , "testInsert" ~: sampleInsertedBoard ~=? insert sampleBoard (1, 2) 1
            , "testValidateBoard" ~: True ~=? validateBoard sampleBoard
            , "testValidateBadBoard" ~: False ~=? validateBoard (insert sampleBoard (0, 0) 7)
            , "testIsFull" ~: True ~=? isFull (initializeBoardFromRows sampleFullRows)
            , "testIsNotFull" ~: False ~=? isFull sampleBoard
            , "testNextEmpty" ~: (0, 0) ~=? findNextEmpty sampleInsertedBoard
            ]
