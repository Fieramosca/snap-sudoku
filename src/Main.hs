{-# LANGUAGE OverloadedStrings #-}
module Main where

import           Data.Aeson            hiding (defaultOptions)
import           Data.ByteString.Char8
import qualified Data.ByteString.Lazy  as Lazy
import           Snap.Core
import           Snap.Http.Server
import           Snap.Util.CORS        (applyCORS, defaultOptions)
import           SudokuBoard           (Board, initializeBoardFromRows, rows,
                                        solve)

main :: IO ()
main = quickHttpServe site

site :: Snap ()
site = applyCORS defaultOptions $ route [ ("solve/:board", sudokuSolver)
                                        , ("/", writeBS "wut")
                                        ]

sudokuSolver :: Snap ()
sudokuSolver = do param <- getParam "board"
                  writeBS . flip (maybe "Need to pass Sudoku Board") param $ either pack id . solvedBoard


solvedBoard :: ByteString -> Either String ByteString
solvedBoard string = do board <- deserializeBoard string
                        case solve board of
                            Just b  -> return . serializeBoard $ b
                            Nothing -> Left "Unsolveable Sudoku"

deserializeBoard :: ByteString -> Either String Board
deserializeBoard string = do board <- eitherDecodeStrict' string
                             return . initializeBoardFromRows $ board

serializeBoard :: Board -> ByteString
serializeBoard = Lazy.toStrict . encode . rows
