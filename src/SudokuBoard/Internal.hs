module SudokuBoard.Internal
( Boxes
, Columns
, Position
, Rows
, insertBox
, insertCol
, insertRow
, makeBoxesFromRows
, makeColumnsFromRows
, validate
, (!)
) where

import           Data.Foldable (toList)
import           Data.Maybe    (fromJust)
import           Data.Sequence (Seq, adjust, chunksOf, filter, fromList, length,
                                mapWithIndex, update, (!?), (><))
import qualified Data.Set      as Set
import           Prelude       hiding (filter, length)

type Position = (Int, Int)

type Rows = Seq (Seq Int)
type Columns = Seq (Seq Int)
type Boxes = Seq (Seq Int)

-- Why the hell do I have to write this?
smap :: (a -> b) -> Seq a -> Seq b
smap f = mapWithIndex (\_ a -> f a)

makeColumnsFromRows :: Rows -> Columns
makeColumnsFromRows rows' = smap (\i -> smap (! i) rows') $ fromList [0..8]

(!) :: Seq a -> Int -> a
(!) s = fromJust . (!?) s

makeBox :: Seq (Seq a) -> Seq (Seq a)
makeBox partition = fromList
    [ (partition ! 0) >< (partition ! 3) >< (partition ! 6)
    , (partition ! 1) >< (partition ! 4) >< (partition ! 7)
    , (partition ! 2) >< (partition ! 5) >< (partition ! 8)
    ]

makeBoxesFromRows :: Rows -> Boxes
makeBoxesFromRows rows' = do
    row <- chunksOf 3 rows'
    makeBox $ do
        splitRow <- row
        chunksOf 3 splitRow

findBox :: Position -> Int
findBox (x, y)
    | x < 3 && y < 3 = 0 | x < 6 && y < 3 = 1 | y < 3 = 2
    | x < 3 && y < 6 = 3 | x < 6 && y < 6 = 4 | y < 6 = 5
    | x < 3 = 6          | x < 6 = 7          | otherwise = 8

findBoxPosition :: Position -> Int
findBoxPosition (x, y) = 3 * yPos + xPos
  where xPos = x `mod` 3
        yPos = y `mod` 3

insertRow :: Rows -> Position -> Int -> Rows
insertRow rows' (x, y) number = adjust (update x number) y rows'

insertCol :: Columns -> Position -> Int -> Columns
insertCol columns' (x, y) number = adjust (update y number) x columns'

insertBox :: Boxes -> Position -> Int -> Boxes
insertBox boxes' position number = adjust (update boxCoordinates number) boxNumber boxes'
  where boxNumber = findBox position
        boxCoordinates = findBoxPosition position

hasNoDuplicates :: Seq Int -> Bool
hasNoDuplicates xs = length xs' == (Set.size . Set.fromList . toList) xs'
  where xs' = filter (/= 0) xs

validate ::  Seq (Seq Int) -> Bool
validate = all hasNoDuplicates
