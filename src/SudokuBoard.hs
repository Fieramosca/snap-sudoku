module SudokuBoard where

import           SudokuBoard.Internal

data Board = Board {rows :: Rows, columns :: Columns, boxes :: Boxes} deriving (Show, Eq)

solve :: Board -> Maybe Board
solve board = solve' [board]
  where solve' [] = Nothing
        solve' (board':rest) = if isFull board' then Just board' else solve' (validBoards ++ rest)
          where newBoards = map (insert board' (findNextEmpty board')) [1..9]
                validBoards = filter validateBoard newBoards

initializeBoardFromRows :: Rows -> Board
initializeBoardFromRows rows' = Board { rows = rows'
                                      , columns = makeColumnsFromRows rows'
                                      , boxes = makeBoxesFromRows rows'
                                      }

boardLookup :: Board -> Position -> Int
boardLookup Board {columns=columns'} (x, y) = columns' ! x ! y

insert :: Board -> Position -> Int -> Board
insert board@Board {rows=rows', columns=columns', boxes=boxes'} position number =
    case boardLookup board position of
        0 -> Board { rows = insertRow rows' position number
                   , columns = insertCol columns' position number
                   , boxes = insertBox boxes' position number
                   }
        _ -> error "already filled"

validateBoard :: Board -> Bool
validateBoard Board {rows=rows', columns=columns', boxes=boxes'} =
    validate rows' && validate columns' && validate boxes'

isFull :: Board -> Bool
isFull Board {rows = rows'} = all (notElem 0) rows'

findNextEmpty :: Board -> Position
findNextEmpty board = if isFull board then error "board is full" else findNextEmpty' board (0, 0)
  where findNextEmpty' board' position =
            case (position, boardLookup board' position) of
                (_, 0)      -> position
                ((8, j), _) -> findNextEmpty' board' (0, j + 1)
                ((i, j), _) -> findNextEmpty' board' (i + 1, j)

